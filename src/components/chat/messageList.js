import React from 'react';

class MessageList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            updateMessage: false,
            messageId: null,
            messageText: ''
        };
    }

    onClickLike = (e) => {
        this.props.setLike(e.target.parentElement.dataset.messageId);
    }

    onUpdateMessage = (e) => {
        const messageId = e.target.parentElement.parentElement.dataset.messageId;
        const message = this.props.getMessage(messageId);

        this.setState({
            updateMessage: true,
            messageId: message.id,
            messageText: message.message
        });
    }

    onClickUpdateMessage = (e) => {
        const messageId = this.state.messageId;
        const messageText = this.state.messageText;

        this.props.updateMessage({ messageId, messageText });

        this.setState({ updateMessage: false });
    }

    onChangeMessageTextUpdate = (e) => {
        const messageText = e.target.value;
        this.setState({ messageText });
    }

    onDeleteMessage = (e) => {
        this.props.deleteMessage(e.target.parentElement.parentElement.dataset.messageId);
    }

    render() {
        return (
            <div className="message-list">
                {this.props.messageList.map((message) =>
                    <div key={message.id} className={`message${message.my ? ' my' : ''}`}>
                        {
                            message.my ?
                                null :
                                <img className="avatar" src={message.avatar} alt="avatar" />
                        }
                        <div className="description">
                            <p className="text">{message.message}</p>
                            <div className="meta-info">
                                <div className="like" data-message-id={message.id}>
                                    <span className="symbol" onClick={this.onClickLike}>&#10004;</span>
                                    <span>{message.likes}</span>
                                    {
                                        message.my ?
                                            <span>
                                                <span className="symbol" onClick={this.onUpdateMessage}>&#9998;</span>
                                                <span className="symbol" onClick={this.onDeleteMessage}>&#9746;</span>
                                            </span> : null
                                    }
                                </div>

                                <div className="date">{message.created_at}</div>
                            </div>
                        </div>
                    </div>
                )}
                {
                    this.state.updateMessage ?
                        <div className="message-input">
                            <textarea placeholder="Enter message..." value={this.state.messageText} onChange={this.onChangeMessageTextUpdate}></textarea>
                            <button className="send" onClick={this.onClickUpdateMessage}>
                                <span className="send-symbol">&#8679;</span> Update
                        </button>
                        </div> : null
                }
            </div>
        );
    }
}

export default MessageList;
