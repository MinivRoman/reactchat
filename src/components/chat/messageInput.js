import React from 'react';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: 'Roman',
            likes: 0,
            message: '',
            my: true
        }
    }

    onChangeMessage = (e) => {
        const message = e.target.value;
        this.setState({ message })
    }

    onClickSend = (e) => {
        this.setState({ message: '' });

        const message = Object.assign({}, this.state);
        this.props.setMessage(message);
    }

    render() {
        return (
            <div className="message-input">
                <textarea placeholder="Enter message..." value={this.state.message} onChange={this.onChangeMessage}></textarea>
                <button className="send" onClick={this.onClickSend}>
                    <span className="send-symbol">&#8679;</span> Send
                </button>
            </div>
        );
    }
}

export default MessageInput;
