import React from 'react';
import Loader from '../loader';
import ChatView from './view';
import './chat.css';

class Chat extends React.Component {
    componentDidMount() {
        fetch('https://api.myjson.com/bins/1hiqin')
            .then((res) => res.json())
            .then((messageList) => {
                messageList = messageList.map(message => {
                    message.likes = 0;
                    return message;
                });
                this.setState({ messageList });
            });
    }

    constructor(props) {
        super(props);

        this.state = {
            messageList: null
        };
    }

    setTimeFormat(unitTime) {
        return `0${unitTime}`.slice(-2);
    }

    setMessageDate(date) {
        const seconds = this.setTimeFormat(date.getSeconds());
        const minutes = this.setTimeFormat(date.getMinutes());
        const hours = this.setTimeFormat(date.getHours());
        const day = this.setTimeFormat(date.getDate());
        const month = this.setTimeFormat(date.getMonth() + 1);
        const year = date.getFullYear();

        const messageDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
        return messageDate;
    }

    setMessage = (message) => {
        const { messageList } = this.state;
        
        message.created_at = this.setMessageDate(new Date());
        message.id = String(messageList[messageList.length - 1].id - 1);
        
        messageList.push(message);

        this.setState({ messageList });
    }

    setLike = (messageId) => {
        const messageList = this.state.messageList.map((message) => {
            if (message.id === messageId && !message.my) {
                message.likes++;
            }
            return message;
        });

        this.setState({ messageList });
    }

    updateMessage = (messageOptions) => {
        const { messageId, messageText } = messageOptions;
        
        const messageList = this.state.messageList.map((message) => {
            if (message.id === messageId && message.my) {
                message.message = messageText;
            }
            return message;
        });

        this.setState({ messageList });
    }

    getMessage = (messageId) => {
        return this.state.messageList.find((message) => message.id === messageId);
    }

    deleteMessage = (messageId) => {
        const { messageList } = this.state; 
        
        const messageIndex = messageList.indexOf(messageList.find((message) => message.id === messageId));
        messageList.splice(messageIndex, 1);
        
        this.setState({ messageList });
    }

    render() {
        return (
            this.state.messageList ?
                <ChatView
                    messageList={this.state.messageList}
                    setMessage={this.setMessage}
                    getMessage={this.getMessage}
                    setLike={this.setLike}
                    updateMessage={this.updateMessage}
                    deleteMessage={this.deleteMessage}
                /> :
                <Loader />
        );
    }
}

export default Chat;