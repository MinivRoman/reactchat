import React from 'react';

class ChatView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header>
                <div className="title">Chat room</div>
                <div className="number-participants"><span>{this.props.numberParticipants}</span> participants</div>
                <div className="number-messages"><span>{this.props.numberMessages}</span> messages</div>
                <div className="last-message">last message at {this.props.timeLastMessage}</div>
            </header>
        );
    }
}

export default ChatView;
