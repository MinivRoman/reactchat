import React from 'react';
import Header from './header';
import MessageList from './messageList';
import MessageInput from './messageInput';

class ChatView extends React.Component {
    constructor(props) {
        super(props);
    }

    getNumberParticipants() {
        const userNameList = this.props.messageList.map((message) => {
            return message.user;
        });

        const numberParticipants = [...new Set(userNameList)].length;

        return numberParticipants;
    }

    getNumberMessages() {
        return this.props.messageList.length;
    }

    getTimeLastMessage() {
        const { messageList } = this.props;
        return messageList[messageList.length - 1].created_at.split(' ')[1];
    }

    render() {
        return (
            <div className="chat">
                <Header
                    numberParticipants={this.getNumberParticipants()}
                    numberMessages={this.getNumberMessages()}
                    timeLastMessage={this.getTimeLastMessage()}
                />
                <MessageList
                    messageList={this.props.messageList}
                    setLike={this.props.setLike}
                    getMessage={this.props.getMessage}
                    updateMessage={this.props.updateMessage}
                    deleteMessage={this.props.deleteMessage}
                />
                <MessageInput setMessage={this.props.setMessage} />
            </div>
        );
    }
}

export default ChatView;
